

angular.module('ordyeducationui').controller('EditExerciceController', function($scope, $routeParams, $location, ExerciceResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.exercice = new ExerciceResource(self.original);
        };
        var errorCallback = function() {
            $location.path("/Exercices");
        };
        ExerciceResource.get({ExerciceId:$routeParams.ExerciceId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.exercice);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.exercice.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Exercices");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Exercices");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.exercice.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});