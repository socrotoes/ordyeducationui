
angular.module('ordyeducationui').controller('NewUtilisateurController', function ($scope, $location, locationParser, UtilisateurResource , PosteResource, AgenceResource, ExerciceResource ,ngToast) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.utilisateur = $scope.utilisateur || {};
    
    $scope.posteList = PosteResource.queryAll(function(items){
        $scope.posteSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.nomPoste
            });
        });
    });
    $scope.$watch("posteSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.utilisateur.poste = {};
            $scope.utilisateur.poste.id = selection.value;
        }
    });
    
    $scope.agenceList = AgenceResource.queryAll(function(items){
        $scope.agenceSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.nomAgence
            });
        });
    });
    $scope.$watch("agenceSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.utilisateur.agence = {};
            $scope.utilisateur.agence.id = selection.value;
        }
    });
    
    $scope.exerciceList = ExerciceResource.queryAll(function(items){
        $scope.exerciceSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.annee
            });
        });
    });
    $scope.$watch("exerciceSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.utilisateur.exercice = {};
            $scope.utilisateur.exercice.id = selection.value;
        }
    });
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Utilisateurs/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        UtilisateurResource.save($scope.utilisateur, successCallback, errorCallback);
          ngToast.create('Enregistrer avec success');
    };
    
    $scope.cancel = function() {
        $location.path("/Utilisateurs");
    };
});