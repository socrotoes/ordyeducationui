
angular.module('ordyeducationui').controller('NewExerciceController', function ($scope, $location, locationParser, ExerciceResource ,ngToast ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.exercice = $scope.exercice || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Exercices/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        ExerciceResource.save($scope.exercice, successCallback, errorCallback);
          ngToast.create('Enregistrer avec success');
    };
    
    $scope.cancel = function() {
        $location.path("/Exercices");
    };
});