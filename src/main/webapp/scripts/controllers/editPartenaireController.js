

angular.module('ordyeducationui').controller('EditPartenaireController', function($scope, $routeParams, $location, PartenaireResource , UtilisateurResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.partenaire = new PartenaireResource(self.original);
            UtilisateurResource.queryAll(function(items) {
                $scope.inviterParSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.nom
                    };
                    if($scope.partenaire.inviterPar && item.id == $scope.partenaire.inviterPar.id) {
                        $scope.inviterParSelection = labelObject;
                        $scope.partenaire.inviterPar = wrappedObject;
                        self.original.inviterPar = $scope.partenaire.inviterPar;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            $location.path("/Partenaires");
        };
        PartenaireResource.get({PartenaireId:$routeParams.PartenaireId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.partenaire);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.partenaire.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Partenaires");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Partenaires");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.partenaire.$remove(successCallback, errorCallback);
    };
    
    $scope.$watch("inviterParSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.partenaire.inviterPar = {};
            $scope.partenaire.inviterPar.id = selection.value;
        }
    });
    
    $scope.get();
});