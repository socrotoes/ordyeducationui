
angular.module('ordyeducationui').controller('NewMessageController', function ($scope, $location, locationParser, MessageResource ,ngToast) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.message = $scope.message || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Messages/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        MessageResource.save($scope.message, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/Messages");
    };
});