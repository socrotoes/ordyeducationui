

angular.module('ordyeducationui').controller('EditPosteController', function($scope, $routeParams, $location, PosteResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.poste = new PosteResource(self.original);
        };
        var errorCallback = function() {
            $location.path("/Postes");
        };
        PosteResource.get({PosteId:$routeParams.PosteId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.poste);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.poste.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Postes");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Postes");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.poste.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});