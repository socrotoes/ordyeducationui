
angular.module('ordyeducationui').controller('NewEtablissementController', function ($scope, $location, locationParser, EtablissementResource ,ngToast ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.etablissement = $scope.etablissement || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Etablissements/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        EtablissementResource.save($scope.etablissement, successCallback, errorCallback);
          ngToast.create('Enregistrer avec success');
    };
    
    $scope.cancel = function() {
        $location.path("/Etablissements");
    };
});