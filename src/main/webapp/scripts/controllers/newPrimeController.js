
angular.module('ordyeducationui').controller('NewPrimeController', function ($scope, $location, locationParser, PrimeResource ,ngToast) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.prime = $scope.prime || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Primes/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        PrimeResource.save($scope.prime, successCallback, errorCallback);
          ngToast.create('Enregistrer avec success');
    };
    
    $scope.cancel = function() {
        $location.path("/Primes");
    };
});