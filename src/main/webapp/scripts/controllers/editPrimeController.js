

angular.module('ordyeducationui').controller('EditPrimeController', function($scope, $routeParams, $location, PrimeResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.prime = new PrimeResource(self.original);
        };
        var errorCallback = function() {
            $location.path("/Primes");
        };
        PrimeResource.get({PrimeId:$routeParams.PrimeId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.prime);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.prime.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Primes");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Primes");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.prime.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});