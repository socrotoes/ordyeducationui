
angular.module('ordyeducationui').controller('UtilisateurConnectController', function ($scope, $location, locationParser, UtilisateurResource) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.utilisateur = {};
    
    var utilisateurs = [
                 {
                     "pseudo": 'admin',
                     "password": '00B37A34C9'
                                        
                 },
                 {
                	 "pseudo": 'coordonnateur',
                     "password": '00B37A34C1'
                   
                 }
             ];
    
    $scope.utilisateurs = utilisateurs;

    $scope.login = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Utilisateurs/connect/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        UtilisateurConnectResource.login($scope.utilisateur, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/Utilisateurs");
    };
});