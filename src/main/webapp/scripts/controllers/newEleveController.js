
angular.module('ordyeducationui').controller('NewEleveController', function ($scope, $location, locationParser, EleveResource , ClasseResource, EtablissementResource ,ngToast ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.eleve = $scope.eleve || {};
    
    $scope.classeList = ClasseResource.queryAll(function(items){
        $scope.classeSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.nomClasse
            });
        });
    });
    $scope.$watch("classeSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.eleve.classe = {};
            $scope.eleve.classe.id = selection.value;
        }
    });
    
    $scope.etablissementList = EtablissementResource.queryAll(function(items){
        $scope.etablissementSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.nom
            });
        });
    });
    $scope.$watch("etablissementSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.eleve.etablissement = {};
            $scope.eleve.etablissement.id = selection.value;
        }
    });
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Eleves/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        EleveResource.save($scope.eleve, successCallback, errorCallback);
         ngToast.create('Enregistrer avec success');
    };
    
    $scope.cancel = function() {
        $location.path("/Eleves");
    };
});