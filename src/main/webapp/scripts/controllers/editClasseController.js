

angular.module('ordyeducationui').controller('EditClasseController', function($scope, $routeParams, $location, ClasseResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.classe = new ClasseResource(self.original);
        };
        var errorCallback = function() {
            $location.path("/Classes");
        };
        ClasseResource.get({ClasseId:$routeParams.ClasseId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.classe);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.classe.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Classes");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Classes");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.classe.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});