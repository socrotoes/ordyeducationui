

angular.module('ordyeducationui').controller('EditEtablissementController', function($scope, $routeParams, $location, EtablissementResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.etablissement = new EtablissementResource(self.original);
        };
        var errorCallback = function() {
            $location.path("/Etablissements");
        };
        EtablissementResource.get({EtablissementId:$routeParams.EtablissementId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.etablissement);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.etablissement.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Etablissements");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Etablissements");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.etablissement.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});