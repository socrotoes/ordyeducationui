
angular.module('ordyeducationui').controller('NewPartenaireController', function ($scope, $location, locationParser, PartenaireResource , UtilisateurResource, ngToast) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.partenaire = $scope.partenaire || {};
    
    $scope.inviterParList = UtilisateurResource.queryAll(function(items){
        $scope.inviterParSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.nom
            });
        });
    });
    $scope.$watch("inviterParSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.partenaire.inviterPar = {};
            $scope.partenaire.inviterPar.id = selection.value;
        }
    });
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Partenaires/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        PartenaireResource.save($scope.partenaire, successCallback, errorCallback);
          ngToast.create('Enregistrer avec success');
    };
    
    $scope.cancel = function() {
        $location.path("/Partenaires");
    };
});