angular.module('ordyeducationui').factory('ExerciceResource', function($resource){
    var resource = $resource('rest/exercices/:ExerciceId',{ExerciceId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});