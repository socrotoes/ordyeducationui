angular.module('ordyeducationui').factory('PosteResource', function($resource){
    var resource = $resource('rest/postes/:PosteId',{PosteId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});