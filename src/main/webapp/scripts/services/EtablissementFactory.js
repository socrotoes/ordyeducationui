angular.module('ordyeducationui').factory('EtablissementResource', function($resource){
    var resource = $resource('rest/etablissements/:EtablissementId',{EtablissementId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});