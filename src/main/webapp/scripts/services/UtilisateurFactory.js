angular.module('ordyeducationui').factory('UtilisateurResource', function($resource){
    var resource = $resource('rest/utilisateurs/:UtilisateurId',{UtilisateurId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});

angular.module('ordyeducationui').factory('UtilisateurConnectResource', function($resource){
    var resource = $resource('rest/utilisateurs/connect:UtilisateurId',{UtilisateurId:'@id'},{'queryAll':{method:'GET',isArray:true}});
    return resource;
});