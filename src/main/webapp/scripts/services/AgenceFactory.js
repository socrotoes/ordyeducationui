angular.module('ordyeducationui').factory('AgenceResource', function($resource){
    var resource = $resource('rest/agences/:AgenceId',{AgenceId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});