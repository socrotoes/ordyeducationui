package org.ordyeducationui.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.ordyeducationui.model.Partenaire;
import org.ordyeducationui.model.Utilisateur;



@Stateless
public class PartenaireEJB {
	@PersistenceContext(unitName = "ordyeducationui-persistence-unit")
	private EntityManager em;

	public List<Partenaire> findPartenaires() {
		TypedQuery<Partenaire> query = em.createQuery("SELECT p FROM Partenaire p ORDER BY p.id", Partenaire.class);
		return query.getResultList();
	}

	public List<Partenaire> findByUtilisateur(Utilisateur utilisateur){   
		TypedQuery<Partenaire> query = em.createQuery("SELECT p FROM Partenaire p WHERE p.inviterPar = :inviterPar ", Partenaire.class);
		query.setParameter("inviterPar",utilisateur);
		return query.getResultList();

	}


	public Partenaire findPartenaireById(Long id) {
		return em.find(Partenaire.class, id);
	}

	public Partenaire createPartenaire(Partenaire partenaire) {
		em.persist(partenaire);
		return partenaire;
	}


	public void deletePartenaire(Partenaire partenaire) {
		em.remove(em.merge(partenaire));	

	}
	public Partenaire updatePartenaire(Partenaire partenaire) {
		return em.merge(partenaire);
	}
}