package org.ordyeducationui.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.ordyeducationui.model.Etablissement;

@Stateless
public class EtablissementEJB {
	@PersistenceContext(unitName = "ordyeducationui-persistence-unit")
	private EntityManager em;

	public List<Etablissement> findEtablissements() {
		TypedQuery<Etablissement> query = em.createQuery("SELECT e FROM Etablissement e ORDER BY e.id", Etablissement.class);
		return query.getResultList();
	}

	public Etablissement findEtablissementById(Long id) {
		return em.find(Etablissement.class, id);
	}

	public Etablissement createEtablissement(Etablissement etablissement) {
		em.persist(etablissement);
		return etablissement;
	}


	public void deleteEtablissement(Etablissement etablissement) {
		em.remove(em.merge(etablissement));	

	}
	public Etablissement updateEtablissement(Etablissement etablissement) {
		return em.merge(etablissement);
	}

}
