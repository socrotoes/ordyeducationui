package org.ordyeducationui.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.ordyeducationui.model.Eleve;

@Stateless
public class EleveEJB {
	@PersistenceContext(unitName = "ordyeducationui-persistence-unit")
	private EntityManager em;

	public List<Eleve> findEleves() {
		TypedQuery<Eleve> query = em.createQuery("SELECT e FROM Eleve e ORDER BY e.id", Eleve.class);
		return query.getResultList();
	}
    
public List<Eleve> findElevesByTelephone() {
TypedQuery<Eleve> query = em.createQuery("SELECT e.mtn FROM Eleve e WHERE e.classe.id = :id AND e.etablissement.id= :id", Eleve.class);
		return query.getResultList();
	}

	public Eleve findEleveById(Long id) {
		return em.find(Eleve.class, id);
	}

	public Eleve createEleve(Eleve eleve) {
		em.persist(eleve);
		return eleve;
	}


	public void deleteEleve(Eleve eleve) {
		em.remove(em.merge(eleve));	

	}
	public Eleve updateEleve(Eleve eleve) {
		return em.merge(eleve);
	}

}
