package org.ordyeducationui.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.ordyeducationui.model.Utilisateur;



@Stateless
public class UtilisateurEJB {
	@PersistenceContext(unitName = "ordyeducationui-persistence-unit")
	private EntityManager em;

	public List<Utilisateur> findUtilisateurs() {
		TypedQuery<Utilisateur> query = em.createQuery("SELECT u FROM Utilisateur u ORDER BY u.id", Utilisateur.class);
		return query.getResultList();
	}

	public Utilisateur findUtilisateurById(Long id) {
		return em.find(Utilisateur.class, id);
	}

	public Utilisateur createUtilisateur(Utilisateur utilisateur) {
		em.persist(utilisateur);
		return utilisateur;
	}


	public void deleteUtilisateur(Utilisateur utilisateur) {
		em.remove(em.merge(utilisateur));	

	}
	public Utilisateur updateUtilisateur(Utilisateur utilisateur) {
		return em.merge(utilisateur);
	}
}
