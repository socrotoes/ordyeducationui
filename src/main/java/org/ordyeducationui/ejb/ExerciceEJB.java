package org.ordyeducationui.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.ordyeducationui.model.Exercice;



@Stateless
public class ExerciceEJB {
	@PersistenceContext(unitName = "ordyeducationui-persistence-unit")
	private EntityManager em;

	public List<Exercice> findEleves() {
		TypedQuery<Exercice> query = em.createQuery("SELECT e FROM Exercice e ORDER BY e.id", Exercice.class);
		return query.getResultList();
	}

	public Exercice findExerciceById(Long id) {
		return em.find(Exercice.class, id);
	}

	public Exercice createExercice(Exercice exercice) {
		em.persist(exercice);
		return exercice;
	}


	public void deleteExercice(Exercice exercice) {
		em.remove(em.merge(exercice));	

	}
	public Exercice updateExercice(Exercice exercice) {
		return em.merge(exercice);
	}


}
