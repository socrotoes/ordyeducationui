package org.ordyeducationui.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.ordyeducationui.model.Agence;

@Stateless
public class AgenceEJB {
	@PersistenceContext(unitName = "ordyeducationui-persistence-unit")
	private EntityManager em;

	public List<Agence> findAgences() {
		TypedQuery<Agence> query = em.createQuery("SELECT a FROM Agence a ORDER BY a.id", Agence.class);
		return query.getResultList();
	}

	public Agence findAgenceById(Long id) {
		return em.find(Agence.class, id);
	}

	public Agence createAgence(Agence agence) {
		em.persist(agence);
		return agence;
	}


	public void deleteAgence(Agence agence) {
		em.remove(em.merge(agence));	

	}
	public Agence updateAgence(Agence agence) {
		return em.merge(agence);
	}

}
