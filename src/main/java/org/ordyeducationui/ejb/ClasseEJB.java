package org.ordyeducationui.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.ordyeducationui.model.Classe;

@Stateless
public class ClasseEJB {
	@PersistenceContext(unitName = "ordyeducationui-persistence-unit")
	private EntityManager em;

	public List<Classe> findClasses() {
		TypedQuery<Classe> query = em.createQuery("SELECT a FROM Classe a ORDER BY a.id", Classe.class);
		return query.getResultList();
	}

	public Classe findClasseById(Long id) {
		return em.find(Classe.class, id);
	}

	public Classe createClasse(Classe classe) {
		em.persist(classe);
		return classe;
	}

	public void deleteClasse(Classe classe) {
		em.remove(em.merge(classe));	

	}
	public Classe updateClasse(Classe classe) {
		return em.merge(classe);
	}

}
