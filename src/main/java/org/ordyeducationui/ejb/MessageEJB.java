package org.ordyeducationui.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.ordyeducationui.model.Message;



@Stateless
public class MessageEJB {
	@PersistenceContext(unitName = "ordyeducationui-persistence-unit")
	private EntityManager em;

	public List<Message> findMessages() {
		TypedQuery<Message> query = em.createQuery("SELECT m FROM Message m ORDER BY m.id", Message.class);
		return query.getResultList();
	}

	public Message findMessageById(Long id) {
		return em.find(Message.class, id);
	}

	public Message createMessage(Message message) {
		em.persist(message);
		return message;
	}

	public void deleteMessage(Message message) {
		em.remove(em.merge(message));	

	}
	public Message updateMessage(Message message) {
		return em.merge(message);
	}
}
