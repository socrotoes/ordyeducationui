package org.ordyeducationui.model;

import javax.persistence.Entity;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Transient;
import javax.persistence.Version;

import java.lang.Override;

import org.ordyeducationui.model.Poste;

import javax.persistence.OneToOne;

import org.ordyeducationui.model.Agence;
import org.ordyeducationui.model.Exercice;

import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Utilisateur implements Serializable
{

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", updatable = false, nullable = false)
   private Long id;
   @Version
   @Column(name = "version")
   private int version;

   @Column
   private String nom;

   @Column
   private String pseudo;

   @Column
   private String password;

   @Column
   private String email;

   @Column
   private String mtn;

   @Column
   private String orange;

   @Column
   private String sexe;

   @Transient
   private Integer nbrePartenaire;


   public Integer getNbrePartenaire() {
	return nbrePartenaire;
}

public void setNbrePartenaire(Integer nbrePartenaire) {
	this.nbrePartenaire = nbrePartenaire;
}

@Column
   private String dateNaissance;

   @OneToOne
   private Poste poste;

   @OneToOne
   private Agence agence;

   @OneToOne
   private Exercice exercice;

   public Long getId()
   {
      return this.id;
   }

   public void setId(final Long id)
   {
      this.id = id;
   }

   public int getVersion()
   {
      return this.version;
   }

   public void setVersion(final int version)
   {
      this.version = version;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
      {
         return true;
      }
      if (!(obj instanceof Utilisateur))
      {
         return false;
      }
      Utilisateur other = (Utilisateur) obj;
      if (id != null)
      {
         if (!id.equals(other.id))
         {
            return false;
         }
      }
      return true;
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      return result;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public String getPseudo()
   {
      return pseudo;
   }

   public void setPseudo(String pseudo)
   {
      this.pseudo = pseudo;
   }

   public String getPassword()
   {
      return password;
   }

   public void setPassword(String password)
   {
      this.password = password;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public String getMtn()
   {
      return mtn;
   }

   public void setMtn(String mtn)
   {
      this.mtn = mtn;
   }

   public String getOrange()
   {
      return orange;
   }

   public void setOrange(String orange)
   {
      this.orange = orange;
   }

   public String getSexe()
   {
      return sexe;
   }

   public void setSexe(String sexe)
   {
      this.sexe = sexe;
   }

   public String getDateNaissance()
   {
      return dateNaissance;
   }

   public void setDateNaissance(String dateNaissance)
   {
      this.dateNaissance = dateNaissance;
   }

   public Poste getPoste()
   {
      return poste;
   }

   public void setPoste(Poste poste)
   {
      this.poste = poste;
   }

   public Agence getAgence()
   {
      return agence;
   }

   public void setAgence(Agence agence)
   {
      this.agence = agence;
   }

   public Exercice getExercice()
   {
      return exercice;
   }

   public void setExercice(Exercice exercice)
   {
      this.exercice = exercice;
   }

   @Override
   public String toString()
   {
      String result = getClass().getSimpleName() + " ";
      if (nom != null && !nom.trim().isEmpty())
         result += "nom: " + nom;
      if (pseudo != null && !pseudo.trim().isEmpty())
         result += ", pseudo: " + pseudo;
      if (password != null && !password.trim().isEmpty())
         result += ", password: " + password;
      if (email != null && !email.trim().isEmpty())
         result += ", email: " + email;
      if (mtn != null && !mtn.trim().isEmpty())
         result += ", mtn: " + mtn;
      if (orange != null && !orange.trim().isEmpty())
         result += ", orange: " + orange;
      if (sexe != null && !sexe.trim().isEmpty())
         result += ", sexe: " + sexe;
      if (dateNaissance != null && !dateNaissance.trim().isEmpty())
         result += ", dateNaissance: " + dateNaissance;
      return result;
   }
}
