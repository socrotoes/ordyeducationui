package org.ordyeducationui.model;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;
import java.lang.Override;
import org.ordyeducationui.model.Utilisateur;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Partenaire implements Serializable
{

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", updatable = false, nullable = false)
   private Long id;
   @Version
   @Column(name = "version")
   private int version;

   @Column
   private String nom;

   @Column
   private String telephone;

   @Column
   private String ville;

   @Column
   private String dateNaissance;

   @Column
   private String profession;

   @Column
   private String email;

   @OneToOne
   private Utilisateur inviterPar;

   @Column
   private Float part;

   @Column(length = 20000)
   private String notes;

   public Long getId()
   {
      return this.id;
   }

   public void setId(final Long id)
   {
      this.id = id;
   }

   public int getVersion()
   {
      return this.version;
   }

   public void setVersion(final int version)
   {
      this.version = version;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
      {
         return true;
      }
      if (!(obj instanceof Partenaire))
      {
         return false;
      }
      Partenaire other = (Partenaire) obj;
      if (id != null)
      {
         if (!id.equals(other.id))
         {
            return false;
         }
      }
      return true;
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      return result;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public String getTelephone()
   {
      return telephone;
   }

   public void setTelephone(String telephone)
   {
      this.telephone = telephone;
   }

   public String getVille()
   {
      return ville;
   }

   public void setVille(String ville)
   {
      this.ville = ville;
   }

   public String getDateNaissance()
   {
      return dateNaissance;
   }

   public void setDateNaissance(String dateNaissance)
   {
      this.dateNaissance = dateNaissance;
   }

   public String getProfession()
   {
      return profession;
   }

   public void setProfession(String profession)
   {
      this.profession = profession;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public Utilisateur getInviterPar()
   {
      return inviterPar;
   }

   public void setInviterPar(Utilisateur inviterPar)
   {
      this.inviterPar = inviterPar;
   }

   public Float getPart()
   {
      return part;
   }

   public void setPart(Float part)
   {
      this.part = part;
   }

   public String getNotes()
   {
      return notes;
   }

   public void setNotes(String notes)
   {
      this.notes = notes;
   }

   @Override
   public String toString()
   {
      String result = getClass().getSimpleName() + " ";
      if (nom != null && !nom.trim().isEmpty())
         result += "nom: " + nom;
      if (telephone != null && !telephone.trim().isEmpty())
         result += ", telephone: " + telephone;
      if (ville != null && !ville.trim().isEmpty())
         result += ", ville: " + ville;
      if (dateNaissance != null && !dateNaissance.trim().isEmpty())
         result += ", dateNaissance: " + dateNaissance;
      if (profession != null && !profession.trim().isEmpty())
         result += ", profession: " + profession;
      if (email != null && !email.trim().isEmpty())
         result += ", email: " + email;
      if (part != null)
         result += ", part: " + part;
      if (notes != null && !notes.trim().isEmpty())
         result += ", notes: " + notes;
      return result;
   }
}