package org.ordyeducationui.model;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;
import java.lang.Override;
import org.ordyeducationui.model.Classe;
import javax.persistence.OneToOne;
import org.ordyeducationui.model.Etablissement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Eleve implements Serializable
{

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", updatable = false, nullable = false)
   private Long id;
   @Version
   @Column(name = "version")
   private int version;

   @Column
   private String nom;

   @OneToOne
   private Classe classe;

   @OneToOne
   private Etablissement etablissement;

   @Column
   private String parent;

   @Column
   private String email;

   @Column
   private String mtn;

   @Column
   private String orange;

   @Column
   private String profession;

   @Column
   private String residence;

   public Long getId()
   {
      return this.id;
   }

   public void setId(final Long id)
   {
      this.id = id;
   }

   public int getVersion()
   {
      return this.version;
   }

   public void setVersion(final int version)
   {
      this.version = version;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
      {
         return true;
      }
      if (!(obj instanceof Eleve))
      {
         return false;
      }
      Eleve other = (Eleve) obj;
      if (id != null)
      {
         if (!id.equals(other.id))
         {
            return false;
         }
      }
      return true;
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      return result;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public Classe getClasse()
   {
      return classe;
   }

   public void setClasse(Classe classe)
   {
      this.classe = classe;
   }

   public Etablissement getEtablissement()
   {
      return etablissement;
   }

   public void setEtablissement(Etablissement etablissement)
   {
      this.etablissement = etablissement;
   }

   public String getParent()
   {
      return parent;
   }

   public void setParent(String parent)
   {
      this.parent = parent;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public String getMtn()
   {
      return mtn;
   }

   public void setMtn(String mtn)
   {
      this.mtn = mtn;
   }

   public String getOrange()
   {
      return orange;
   }

   public void setOrange(String orange)
   {
      this.orange = orange;
   }

   public String getProfession()
   {
      return profession;
   }

   public void setProfession(String profession)
   {
      this.profession = profession;
   }

   public String getResidence()
   {
      return residence;
   }

   public void setResidence(String residence)
   {
      this.residence = residence;
   }

   @Override
   public String toString()
   {
      String result = getClass().getSimpleName() + " ";
      if (nom != null && !nom.trim().isEmpty())
         result += "nom: " + nom;
      if (parent != null && !parent.trim().isEmpty())
         result += ", parent: " + parent;
      if (email != null && !email.trim().isEmpty())
         result += ", email: " + email;
      if (mtn != null && !mtn.trim().isEmpty())
         result += ", mtn: " + mtn;
      if (orange != null && !orange.trim().isEmpty())
         result += ", orange: " + orange;
      if (profession != null && !profession.trim().isEmpty())
         result += ", profession: " + profession;
      if (residence != null && !residence.trim().isEmpty())
         result += ", residence: " + residence;
      return result;
   }
}