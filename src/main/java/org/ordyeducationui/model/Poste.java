package org.ordyeducationui.model;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;
import java.lang.Override;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Poste implements Serializable
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@Column
	private String nomPoste;

	@Column
	private String departement;

	public Long getId()
	{
		return this.id;
	}

	public void setId(final Long id)
	{
		this.id = id;
	}

	public int getVersion()
	{
		return this.version;
	}

	public void setVersion(final int version)
	{
		this.version = version;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (!(obj instanceof Poste))
		{
			return false;
		}
		Poste other = (Poste) obj;
		if (id != null)
		{
			if (!id.equals(other.id))
			{
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public String getNomPoste()
	{
		return nomPoste;
	}

	public void setNomPoste(String nomPoste)
	{
		this.nomPoste = nomPoste;
	}

	public String getDepartement()
	{
		return departement;
	}

	public void setDepartement(String departement)
	{
		this.departement = departement;
	}

	@Override
	public String toString()
	{
		String result = getClass().getSimpleName() + " ";
		if (nomPoste != null && !nomPoste.trim().isEmpty())
			result += "nomPoste: " + nomPoste;
		if (departement != null && !departement.trim().isEmpty())
			result += ", departement: " + departement;
		return result;
	}
}
