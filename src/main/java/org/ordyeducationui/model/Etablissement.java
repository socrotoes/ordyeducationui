package org.ordyeducationui.model;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;
import java.lang.Override;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Etablissement implements Serializable
{

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Column(name = "id", updatable = false, nullable = false)
   private Long id;
   @Version
   @Column(name = "version")
   private int version;

   @Column
   private String nom;

   @Column
   private String ville;

   @Column
   private String quartier;

   @Column
   private String responsable;

   @Column
   private String email;

   @Column
   private String mtn;

   @Column
   private String orange;

   @Column(length = 20000)
   private String notes;

   public Long getId()
   {
      return this.id;
   }

   public void setId(final Long id)
   {
      this.id = id;
   }

   public int getVersion()
   {
      return this.version;
   }

   public void setVersion(final int version)
   {
      this.version = version;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
      {
         return true;
      }
      if (!(obj instanceof Etablissement))
      {
         return false;
      }
      Etablissement other = (Etablissement) obj;
      if (id != null)
      {
         if (!id.equals(other.id))
         {
            return false;
         }
      }
      return true;
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      return result;
   }

   public String getNom()
   {
      return nom;
   }

   public void setNom(String nom)
   {
      this.nom = nom;
   }

   public String getVille()
   {
      return ville;
   }

   public void setVille(String ville)
   {
      this.ville = ville;
   }

   public String getQuartier()
   {
      return quartier;
   }

   public void setQuartier(String quartier)
   {
      this.quartier = quartier;
   }

   public String getResponsable()
   {
      return responsable;
   }

   public void setResponsable(String responsable)
   {
      this.responsable = responsable;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public String getMtn()
   {
      return mtn;
   }

   public void setMtn(String mtn)
   {
      this.mtn = mtn;
   }

   public String getOrange()
   {
      return orange;
   }

   public void setOrange(String orange)
   {
      this.orange = orange;
   }

   public String getNotes()
   {
      return notes;
   }

   public void setNotes(String notes)
   {
      this.notes = notes;
   }

   @Override
   public String toString()
   {
      String result = getClass().getSimpleName() + " ";
      if (nom != null && !nom.trim().isEmpty())
         result += "nom: " + nom;
      if (ville != null && !ville.trim().isEmpty())
         result += ", ville: " + ville;
      if (quartier != null && !quartier.trim().isEmpty())
         result += ", quartier: " + quartier;
      if (responsable != null && !responsable.trim().isEmpty())
         result += ", responsable: " + responsable;
      if (email != null && !email.trim().isEmpty())
         result += ", email: " + email;
      if (mtn != null && !mtn.trim().isEmpty())
         result += ", mtn: " + mtn;
      if (orange != null && !orange.trim().isEmpty())
         result += ", orange: " + orange;
      if (notes != null && !notes.trim().isEmpty())
         result += ", notes: " + notes;
      return result;
   }
}