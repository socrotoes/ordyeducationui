
ordyeducationui est une application web de gestion d'une association socio-educative

pour deployer l'application vous devez disposer de:

-d'un Systeme d'exploitation Linux ou Mac OS
-JDK 1.7 ou 1.8
-Maven 3.2 ou plus
-Git version 1.9.1
-Le seveur jboss-eap-6.2
-D'un navigateur web comme Mozilla Firefox ou Google Chrome

Suivez les etapes suivantes pour cloner le projet:

dans votre terminal tapez la commande
git clone https://gitlab.com/socrotoes/ordyeducationui.git

Apres le telechargement complet du projet
positionneaz vous a la racine avec la commande
cd ordyeducationui

et tapez la commande suivante pour deployer le projet
./ordyeducationui_deploy.sh

Ensuite tapez l'url suivante dans votre navigateur pour acceder a l'application

http://localhost:8080/ordyeducationui
