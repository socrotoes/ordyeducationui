angular.module('ordyeducationui').factory('PartenaireResource', function($resource){
    var resource = $resource('rest/partenaires/:PartenaireId',{PartenaireId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});