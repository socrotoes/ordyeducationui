angular.module('ordyeducationui').factory('MessageResource', function($resource){
    var resource = $resource('rest/messages/:MessageId',{MessageId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});