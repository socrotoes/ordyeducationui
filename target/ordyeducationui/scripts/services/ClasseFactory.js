angular.module('ordyeducationui').factory('ClasseResource', function($resource){
    var resource = $resource('rest/classes/:ClasseId',{ClasseId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});