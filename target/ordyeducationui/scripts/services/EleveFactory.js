angular.module('ordyeducationui').factory('EleveResource', function($resource){
    var resource = $resource('rest/eleves/:EleveId',{EleveId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});