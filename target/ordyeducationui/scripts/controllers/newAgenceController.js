
angular.module('ordyeducationui').controller('NewAgenceController', function ($scope, $location, locationParser, AgenceResource, ngToast) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.agence = $scope.agence || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Agences/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        AgenceResource.save($scope.agence, successCallback, errorCallback);
         ngToast.create('Enregistrer avec success');
    };
    
    $scope.cancel = function() {
        $location.path("/Agences");
    };
});