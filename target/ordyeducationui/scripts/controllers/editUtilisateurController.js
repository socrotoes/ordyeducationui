

angular.module('ordyeducationui').controller('EditUtilisateurController', function($scope, $routeParams, $location, UtilisateurResource , PosteResource, AgenceResource, ExerciceResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.utilisateur = new UtilisateurResource(self.original);
            PosteResource.queryAll(function(items) {
                $scope.posteSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.nomPoste
                    };
                    if($scope.utilisateur.poste && item.id == $scope.utilisateur.poste.id) {
                        $scope.posteSelection = labelObject;
                        $scope.utilisateur.poste = wrappedObject;
                        self.original.poste = $scope.utilisateur.poste;
                    }
                    return labelObject;
                });
            });
            AgenceResource.queryAll(function(items) {
                $scope.agenceSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.nomAgence
                    };
                    if($scope.utilisateur.agence && item.id == $scope.utilisateur.agence.id) {
                        $scope.agenceSelection = labelObject;
                        $scope.utilisateur.agence = wrappedObject;
                        self.original.agence = $scope.utilisateur.agence;
                    }
                    return labelObject;
                });
            });
            ExerciceResource.queryAll(function(items) {
                $scope.exerciceSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.annee
                    };
                    if($scope.utilisateur.exercice && item.id == $scope.utilisateur.exercice.id) {
                        $scope.exerciceSelection = labelObject;
                        $scope.utilisateur.exercice = wrappedObject;
                        self.original.exercice = $scope.utilisateur.exercice;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            $location.path("/Utilisateurs");
        };
        UtilisateurResource.get({UtilisateurId:$routeParams.UtilisateurId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.utilisateur);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.utilisateur.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Utilisateurs");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Utilisateurs");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.utilisateur.$remove(successCallback, errorCallback);
    };
    
    $scope.$watch("posteSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.utilisateur.poste = {};
            $scope.utilisateur.poste.id = selection.value;
        }
    });
    $scope.$watch("agenceSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.utilisateur.agence = {};
            $scope.utilisateur.agence.id = selection.value;
        }
    });
    $scope.$watch("exerciceSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.utilisateur.exercice = {};
            $scope.utilisateur.exercice.id = selection.value;
        }
    });
    
    $scope.get();
});