

angular.module('ordyeducationui').controller('EditEleveController', function($scope, $routeParams, $location, EleveResource , ClasseResource, EtablissementResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.eleve = new EleveResource(self.original);
            ClasseResource.queryAll(function(items) {
                $scope.classeSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.nomClasse
                    };
                    if($scope.eleve.classe && item.id == $scope.eleve.classe.id) {
                        $scope.classeSelection = labelObject;
                        $scope.eleve.classe = wrappedObject;
                        self.original.classe = $scope.eleve.classe;
                    }
                    return labelObject;
                });
            });
            EtablissementResource.queryAll(function(items) {
                $scope.etablissementSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.nom
                    };
                    if($scope.eleve.etablissement && item.id == $scope.eleve.etablissement.id) {
                        $scope.etablissementSelection = labelObject;
                        $scope.eleve.etablissement = wrappedObject;
                        self.original.etablissement = $scope.eleve.etablissement;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            $location.path("/Eleves");
        };
        EleveResource.get({EleveId:$routeParams.EleveId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.eleve);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.eleve.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Eleves");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Eleves");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.eleve.$remove(successCallback, errorCallback);
    };
    
    $scope.$watch("classeSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.eleve.classe = {};
            $scope.eleve.classe.id = selection.value;
        }
    });
    $scope.$watch("etablissementSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.eleve.etablissement = {};
            $scope.eleve.etablissement.id = selection.value;
        }
    });
    
    $scope.get();
});