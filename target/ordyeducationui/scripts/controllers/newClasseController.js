
angular.module('ordyeducationui').controller('NewClasseController', function ($scope, $location, locationParser, ClasseResource ,ngToast ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.classe = $scope.classe || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Classes/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        ClasseResource.save($scope.classe, successCallback, errorCallback);
          ngToast.create('Enregistrer avec success');
    };
    
    $scope.cancel = function() {
        $location.path("/Classes");
    };
});