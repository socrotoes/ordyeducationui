

angular.module('ordyeducationui').controller('EditMessageController', function($scope, $routeParams, $location, MessageResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.message = new MessageResource(self.original);
        };
        var errorCallback = function() {
            $location.path("/Messages");
        };
        MessageResource.get({MessageId:$routeParams.MessageId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.message);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.message.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Messages");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Messages");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.message.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});