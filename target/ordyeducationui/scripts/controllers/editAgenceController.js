

angular.module('ordyeducationui').controller('EditAgenceController', function($scope, $routeParams, $location, AgenceResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.agence = new AgenceResource(self.original);
        };
        var errorCallback = function() {
            $location.path("/Agences");
        };
        AgenceResource.get({AgenceId:$routeParams.AgenceId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.agence);
    };

    $scope.save = function() {
        var successCallback = function(){
            $scope.get();
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        };
        $scope.agence.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Agences");
    };

    $scope.remove = function() {
        var successCallback = function() {
            $location.path("/Agences");
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError=true;
        }; 
        $scope.agence.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
});