

angular.module('ordyeducationui').controller('SearchEleveController', function($scope, $http, EleveResource , ClasseResource, EtablissementResource) {

    $scope.search={};
    $scope.currentPage = 0;
    $scope.pageSize= 10;
    $scope.searchResults = [];
    $scope.filteredResults = [];
    $scope.pageRange = [];
    $scope.numberOfPages = function() {
        var result = Math.ceil($scope.filteredResults.length/$scope.pageSize);
        var max = (result == 0) ? 1 : result;
        $scope.pageRange = [];
        for(var ctr=0;ctr<max;ctr++) {
            $scope.pageRange.push(ctr);
        }
        return max;
    };
    
    $scope.eleveList = EleveResource.queryAll();
    
    $scope.classeList = ClasseResource.queryAll();
    $scope.etablissementList = EtablissementResource.queryAll();

    $scope.performSearch = function() {
        $scope.searchResults = EleveResource.queryAll(function(){
            $scope.numberOfPages();
        });
    };
    
    $scope.previous = function() {
       if($scope.currentPage > 0) {
           $scope.currentPage--;
       }
    };
    
    $scope.next = function() {
       if($scope.currentPage < ($scope.numberOfPages() - 1) ) {
           $scope.currentPage++;
       }
    };
    
    $scope.setPage = function(n) {
       $scope.currentPage = n;
    };

    $scope.performSearch();
    
    $scope.exportPDF= function(){
          	var doc = new jsPDF();
            doc.setFontSize(20);
            doc.text(35,25,"Liste des numeros de telephones");
            var date = new Date();
            doc.setFontSize(11);
            var index = 0;
            var marginTop = 50;
            var incMargin = 0;
        
             height = doc.drawTable(eleveList, {xstart:10,ystart:10,tablestart:90,marginleft:60});
            doc.save('Telephone--'+date+".pdf");
	   };
    
});