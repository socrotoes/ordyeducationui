
angular.module('ordyeducationui').controller('NewPosteController', function ($scope, $location, locationParser, PosteResource, ngToast ) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.poste = $scope.poste || {};
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            $location.path('/Postes/edit/' + id);
            $scope.displayError = false;
        };
        var errorCallback = function() {
            $scope.displayError = true;
        };
        PosteResource.save($scope.poste, successCallback, errorCallback);
          ngToast.create('Enregistrer avec success');
    };
    
    $scope.cancel = function() {
        $location.path("/Postes");
    };
});