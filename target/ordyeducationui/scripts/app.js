'use strict';

angular.module('ordyeducationui',['ngRoute','ngResource','ngToast'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/',{templateUrl:'views/landing.html',controller:'LandingPageController'})
      .when('/Agences',{templateUrl:'views/Agence/search.html',controller:'SearchAgenceController'})
      .when('/Agences/new',{templateUrl:'views/Agence/detail.html',controller:'NewAgenceController'})
      .when('/Agences/edit/:AgenceId',{templateUrl:'views/Agence/detail.html',controller:'EditAgenceController'})
      .when('/Classes',{templateUrl:'views/Classe/search.html',controller:'SearchClasseController'})
      .when('/Classes/new',{templateUrl:'views/Classe/detail.html',controller:'NewClasseController'})
      .when('/Classes/edit/:ClasseId',{templateUrl:'views/Classe/detail.html',controller:'EditClasseController'})
      .when('/Eleves',{templateUrl:'views/Eleve/search.html',controller:'SearchEleveController'})
      .when('/Eleves/new',{templateUrl:'views/Eleve/detail.html',controller:'NewEleveController'})
      .when('/Eleves/edit/:EleveId',{templateUrl:'views/Eleve/detail.html',controller:'EditEleveController'})
      .when('/Etablissements',{templateUrl:'views/Etablissement/search.html',controller:'SearchEtablissementController'})
      .when('/Etablissements/new',{templateUrl:'views/Etablissement/detail.html',controller:'NewEtablissementController'})
      .when('/Etablissements/edit/:EtablissementId',{templateUrl:'views/Etablissement/detail.html',controller:'EditEtablissementController'})
      .when('/Exercices',{templateUrl:'views/Exercice/search.html',controller:'SearchExerciceController'})
      .when('/Exercices/new',{templateUrl:'views/Exercice/detail.html',controller:'NewExerciceController'})
      .when('/Exercices/edit/:ExerciceId',{templateUrl:'views/Exercice/detail.html',controller:'EditExerciceController'})
      .when('/Messages',{templateUrl:'views/Message/search.html',controller:'SearchMessageController'})
      .when('/Messages/new',{templateUrl:'views/Message/detail.html',controller:'NewMessageController'})
      .when('/Messages/edit/:MessageId',{templateUrl:'views/Message/detail.html',controller:'EditMessageController'})
      .when('/Partenaires',{templateUrl:'views/Partenaire/search.html',controller:'SearchPartenaireController'})
      .when('/Partenaires/new',{templateUrl:'views/Partenaire/detail.html',controller:'NewPartenaireController'})
      .when('/Partenaires/edit/:PartenaireId',{templateUrl:'views/Partenaire/detail.html',controller:'EditPartenaireController'})
      .when('/Postes',{templateUrl:'views/Poste/search.html',controller:'SearchPosteController'})
      .when('/Postes/new',{templateUrl:'views/Poste/detail.html',controller:'NewPosteController'})
      .when('/Postes/edit/:PosteId',{templateUrl:'views/Poste/detail.html',controller:'EditPosteController'})
      .when('/Primes',{templateUrl:'views/Prime/search.html',controller:'SearchPrimeController'})
      .when('/Primes/new',{templateUrl:'views/Prime/detail.html',controller:'NewPrimeController'})
      .when('/Primes/edit/:PrimeId',{templateUrl:'views/Prime/detail.html',controller:'EditPrimeController'})
      .when('/Utilisateurs',{templateUrl:'views/Utilisateur/search.html',controller:'SearchUtilisateurController'})
      .when('/Utilisateurs/new',{templateUrl:'views/Utilisateur/detail.html',controller:'NewUtilisateurController'})
      .when('/Utilisateurs/edit/:UtilisateurId',{templateUrl:'views/Utilisateur/detail.html',controller:'EditUtilisateurController'})
      .otherwise({
        redirectTo: '/'
      });
  }])
  .controller('LandingPageController', function LandingPageController() {
  })
  .controller('NavController', function NavController($scope, $location) {
    $scope.matchesRoute = function(route) {
        var path = $location.path();
        return (path === ("/" + route) || path.indexOf("/" + route + "/") == 0);
    };
  });
